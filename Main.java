import java.util.List;

public class Main {
    public static void main(String[] args) {
        // parse csv file
        DataParser csv = DataParserFactory.getParser("csv")
                .parse(args[0]);
        // use heap to get sort
        CsvItemHeap heap = new CsvItemHeap()
                .heapify(csv.getDataAsList());
        // generate yaml from heap

    }
}
