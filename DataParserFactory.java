public class DataParserFactory {
    public static DataParser getParser(String type) {
        switch (type.toLowerCase()) {
            case "csv":
                return new CsvDataParser();
            default:
                throw new IllegalArgumentException("Parser type:'" + type + "' does not exist.");
        }
    }
}
