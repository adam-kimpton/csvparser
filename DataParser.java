import java.util.List;

public abstract class DataParser {
    abstract DataParser parse(String filename);
    abstract List getDataAsList();
}
