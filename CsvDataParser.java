import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CsvDataParser extends DataParser {
    private List<CsvItem> data;

    public CsvDataParser() {
        data = new ArrayList<>();
    }

    @Override
    DataParser parse(String filename) {
        try {
            File myObj = new File(filename);
            Scanner myReader = new Scanner(myObj);
            myReader.nextLine();
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                parseLine(data);
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred while reading the file: '" + filename + "'.");
            e.printStackTrace();
        }
        return this;
    }

    private void parseLine(String line) {
        CsvItem item = new CsvItem().fromArray(line.split(","));
        data.add(item);
    }

    public List<CsvItem> getDataAsList() {
        return data;
    }
}
