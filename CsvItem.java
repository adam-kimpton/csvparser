public class CsvItem {
    private String firstname;
    private String lastname;
    private String date;
    private int division;
    private int points;
    private String summary;

    public CsvItem fromArray(String[] arr) {
        if (arr.length != 6) {
            throw new IllegalArgumentException("Array should have 6 items in it.");
        }
        try {
            this.firstname = arr[0];
            this.lastname = arr[1];
            this.date = arr[2];
            this.division = Integer.parseInt(arr[3]);
            this.points = Integer.parseInt(arr[4]);
            this.summary = arr[5];
        } catch (Exception e) {
            System.out.println("An error occurred while processing: '" + arr + "'");
            e.printStackTrace();
        }
        return this;
    }

    public String getFirstname() {
        return firstname;
    }

    public CsvItem firstname(String firstname) {
        this.firstname = firstname;
        return this;
    }

    public String getLastname() {
        return lastname;
    }

    public CsvItem lastname(String lastname) {
        this.lastname = lastname;
        return this;
    }

    public String getDate() {
        return date;
    }

    public CsvItem date(String date) {
        this.date = date;
        return this;
    }

    public int getDivision() {
        return division;
    }

    public CsvItem division(int division) {
        this.division = division;
        return this;
    }

    public int getPoints() {
        return points;
    }

    public CsvItem points(int points) {
        this.points = points;
        return this;
    }

    public String getSummary() {
        return summary;
    }

    public CsvItem summary(String summary) {
        this.summary = summary;
        return this;
    }
}
