import java.util.List;

public class CsvItemHeap {
    private CsvItem[] data;
    private final int MAX_CAPACITY = 3;
    private int current_heap_size;

    public CsvItemHeap() {
        data = new CsvItem[MAX_CAPACITY];
        current_heap_size = 0;
    }

    public CsvItemHeap heapify(List<CsvItem> csv) {
        csv.forEach(csvItem -> {
            this.insert(csvItem);
        });
        return this;
    }

    // Look at value on top of the heap.
    public CsvItem peak() {
        return data[0];
    }

    // Look at value on top of the heap, and remove it (and sort if needed).
    public CsvItem pop() {
        return data[0];
    }

    // Add a new item onto the heap (and sort if needed).
    public void insert(CsvItem item) {
        // check if the new item is worse than the worst item in the heap.
        if (this.compareCsvItems(data[2], item) >= 0) {
            return;
        }

        // Don't add new item if there is no space.
        if (current_heap_size >= MAX_CAPACITY) {
            return;
        }

        // First insert the new key at the end.
        int i = current_heap_size;
        data[i] = item;
        current_heap_size++;

        // Fix the min heap property if it is violated.
        while (i != 0 && compareCsvItems(data[2], data[parent(i)]) > 0) {
            CsvItem tmp = data[i];
            data[i] = data[parent(i)];
            data[parent(i)] = tmp;
            i = parent(i);
        }
    }

    private int parent(int i) {
        return (i - 1) / 2;
    }

    private int compareCsvItems(CsvItem x, CsvItem y) {
        if (x == null) {
            return -1;
        }

        if (x.getDivision() < y.getDivision()) {
            return 1;
        } else if (x.getDivision() == y.getDivision()) {
            if (x.getPoints() > y.getPoints()) {
                return 1;
            } else if (x.getPoints() == y.getPoints()) {
                return 0;
            }
        }
        return -1;
    }
}
